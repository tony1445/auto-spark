FROM python:3.10-slim-bullseye

WORKDIR /

COPY ./ /

RUN pip install --no-cache-dir --upgrade -r /requirements.txt

RUN apt update && apt upgrade -y
