from contextlib import asynccontextmanager
from typing import List

import uvicorn
from fastapi import BackgroundTasks, FastAPI, HTTPException
from fastapi.middleware.cors import CORSMiddleware

from app.internal.utils import log
from app.models.base import database
from app.models.cars import cars
from app.schemas.response import CarsOut
from app.scraper.auto import scrape_auto_bg
from app.scraper.mobile import scrape_mobile_bg


@asynccontextmanager
async def lifespan(app: FastAPI):
    # On startup
    await database.connect()
    yield
    # On shutdown
    await database.disconnect()


app = FastAPI(
    lifespan=lifespan,
    swagger_ui_parameters={"defaultModelsExpandDepth": -1},
)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/reload")
async def reload(background_tasks: BackgroundTasks):
    # Send scraping to background
    background_tasks.add_task(scrape_auto_bg)
    background_tasks.add_task(scrape_mobile_bg)
    return {"message": "Scraper started"}


@app.get("/cars", response_model=List[CarsOut])
async def get_all_suspects():
    try:
        all_suspects = await database.fetch_all(cars.select())
        if all_suspects:
            return all_suspects
        raise HTTPException(
            status_code=400,
            detail="No cars data, please send request to /reload endpoint to start the scraper and try again.",
        )

    except HTTPException as e:
        raise HTTPException(e.status_code, e.detail)

    except Exception as e:
        log(f"Error from suspect endpoint - {e}")
        raise HTTPException(
            status_code=400, detail="No suspect data or other error ocurred."
        )


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
