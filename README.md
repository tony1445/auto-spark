# Auto Spark

Scraper app and api to get listings from popular auto sales bg sites

Sample JSON object
```
{
    "uuid": "ef426953-4065-466f-aeca-7e495bc9e1d8",
    "img": "https://mobistatic1.focus.bg/mobile/photosorg/135/1/11715970932402135_FD.jpg",
    "link": "https://www.auto.bg/obiava/12971965/bmw-118-122ks-6sk",
    "title": "BMW 118 122кс. 6ск.",
    "car_date": " август 2006 г. ",
    "gearbox": "Ръчна",
    "fuel": "Дизел",
    "kilometers": 214000,
    "horsepower": 122,
    "price": 7500,
    "currency": "лв.",
    "source": "auto_bg",
    "created_at": "2024-05-17T18:37:54.417812Z"
}
```