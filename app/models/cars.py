from datetime import datetime

import sqlalchemy
from sqlalchemy.dialects.postgresql import UUID

from app.models.base import metadata

cars = sqlalchemy.Table(
    "cars",
    metadata,
    sqlalchemy.Column(
        "uuid",
        UUID(as_uuid=True),
        primary_key=True,
        default=sqlalchemy.text("uuid_generate_v4()"),
    ),
    sqlalchemy.Column("img", sqlalchemy.String(255), default=""),
    sqlalchemy.Column("link", sqlalchemy.String(255), default="", unique=True),
    sqlalchemy.Column("title", sqlalchemy.String(255), default=""),
    sqlalchemy.Column("car_date", sqlalchemy.String(255), default=""),
    sqlalchemy.Column("gearbox", sqlalchemy.String(255), default=""),
    sqlalchemy.Column("fuel", sqlalchemy.String(255), default=""),
    sqlalchemy.Column("kilometers", sqlalchemy.Integer(), default=1),
    sqlalchemy.Column("horsepower", sqlalchemy.Integer(), default=1),
    sqlalchemy.Column("price", sqlalchemy.Integer(), default=1),
    sqlalchemy.Column("currency", sqlalchemy.String(255), default=""),
    sqlalchemy.Column("source", sqlalchemy.String(255), default=""),
    sqlalchemy.Column(
        "created_at",
        sqlalchemy.DateTime(timezone=True),
        default=datetime.utcnow(),
        nullable=False,
    ),
)
