import databases
import sqlalchemy

from app.models.constants import DATABASE_URL

database = databases.Database(DATABASE_URL)
metadata = sqlalchemy.MetaData()
