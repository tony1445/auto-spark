import os
from datetime import datetime

from app.internal.constants import LOG_ERROR_FILE, LOG_INFO_FILE


def log(info: str, is_error: bool = False) -> None:
    """Function to write logs to files

    Args:
        info (str): log message
        is_error (bool, optional): error message flag. Defaults to False.
    """
    formatted_time = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    log_type = "ERROR" if is_error else "INFO"
    # Standard logging events
    log_file = LOG_INFO_FILE

    # Change the log file if we have error events
    if is_error:
        log_file = LOG_ERROR_FILE

    if not os.path.exists(log_file):
        with open(LOG_INFO_FILE, "w") as f:
            pass

    with open(log_file, "a+") as f:
        f.write(f"{formatted_time}\t{log_type} - {info}\n")

    return
