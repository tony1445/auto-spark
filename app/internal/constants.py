import os

SCRIPT_PATH = os.path.dirname(os.path.realpath(__file__))

# /app
APP_DIR = os.path.dirname(SCRIPT_PATH)

# /app/logs
LOG_DIR = os.path.join(APP_DIR, "logs")

# /app/logs/info.log
LOG_INFO_FILE = os.path.join(LOG_DIR, "info.log")

# /app/logs/error.log
LOG_ERROR_FILE = os.path.join(LOG_DIR, "error.log")
