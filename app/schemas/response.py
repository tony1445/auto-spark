import uuid
from datetime import datetime

from pydantic import BaseModel


class CarsOut(BaseModel):
    uuid: uuid.UUID
    img: str
    link: str
    title: str
    car_date: str
    gearbox: str
    fuel: str
    kilometers: int
    horsepower: int
    price: int
    currency: str
    source: str
    created_at: datetime
