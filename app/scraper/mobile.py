from datetime import datetime

import requests
from bs4 import BeautifulSoup
from price_parser import Price

from app.models.base import database
from app.models.cars import cars
from app.internal.utils import log

async def scrape_mobile_bg() -> None:
    log("Scraper started")
    for i in range(10):
        URL = "https://www.mobile.bg/obiavi/avtomobili-dzhipove/"
        if i > 0:
            URL += f"p-{i+1}"
        with requests.get(URL) as page:
            soup = BeautifulSoup(page.content, "html.parser")
            posts = soup.find_all("form")

            for post in posts:
                action = post.get("action")
                if "mobile.cgi" in action:
                    tables = post.find_all("table", class_="tablereset")
                    for table in tables:
                        try:
                            created_at = datetime.utcnow()
                            img = "https:" + table.find("img", class_="noborder").get(
                                "src"
                            )
                            title = table.find("a", class_="mmmL").text.strip().title()
                            link = "https:" + table.find("a", class_="mmmL").get("href")
                            price_span = table.find("span", class_="price").text.strip()
                            price_parced = Price.fromstring(price_span)
                            price = int(price_parced.amount)
                            currency = price_parced.currency
                            car_data = {
                                "img": img,
                                "price": price,
                                "currency": currency,
                                "created_at": created_at,
                                "title": title,
                                "link": link,
                                "source": "mobile_bg",
                            }

                            log(f"Data extracted - {car_data}")
                            uuid_ = await database.execute(
                                cars.insert().values(**car_data)
                            )
                            log(f"UUID from database - {uuid_}")
                        except Exception as e:
                            log(f"Error from extraction - {e}", is_error=True)
                            continue
