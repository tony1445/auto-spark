from datetime import datetime

import requests
from bs4 import BeautifulSoup
from price_parser import Price

from app.models.base import database
from app.models.cars import cars
from app.internal.utils import log


async def scrape_auto_bg() -> None:
    log("Scraper started")
    for i in range(10):
        URL = "https://www.auto.bg/obiavi/avtomobili-dzhipove"
        if i > 0:
            URL += f"/page/{i+1}"
        with requests.get(URL) as page:
            soup = BeautifulSoup(page.content, "html.parser")
            posts = soup.find_all("div", class_="page")

            for post in posts:
                right_column = post.find("li", class_="rightColumn")
                results = right_column.find("div", class_="results")
                for result in results:
                    try:
                        if result.get("class")[0] != "resultItem":
                            continue
                        elif result.get("class")[0] == "resultItem":
                            created_at = datetime.utcnow()
                            img = "https:" + result.find("li", class_="photo").find(
                                "img"
                            ).get("src")
                            title = result.find("div", class_="link").text.strip()
                            info = result.find("div", class_="info").find_all("strong")
                            link = result.find("a").get("href")
                            price_div = (
                                result.find("div", class_="price")
                                .text.strip()
                                .split("\n")[0]
                            )
                            price_parced = Price.fromstring(price_div)
                            price = int(price_parced.amount)
                            currency = price_parced.currency
                            car_data = {
                                "img": img,
                                "price": price,
                                "currency": currency,
                                "created_at": created_at,
                                "title": title,
                                "link": link,
                                "source": "auto_bg",
                            }
                            if info:
                                parced_kilometers = Price.fromstring(info[1].text)
                                parced_hp = Price.fromstring(info[4].text)
                                car_data.update(
                                    {
                                        "car_date": info[0].text,
                                        "gearbox": info[2].text,
                                        "fuel": info[3].text,
                                        "kilometers": int(parced_kilometers.amount),
                                        "horsepower": int(parced_hp.amount),
                                    }
                                )
                            log(f"Data extracted - {car_data}")
                            uuid_ = await database.execute(
                                cars.insert().values(**car_data)
                            )
                            log(f"UUID from database - {uuid_}")
                    except Exception as e:
                        log(f"Error from extraction - {e}", is_error=True)
                        continue
